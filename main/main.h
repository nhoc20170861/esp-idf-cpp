#pragma once

#include <iostream>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <cmath>
#include "nvs_flash.h"

#include "cppgpio.h"
#include "uart.h"
#include "cJSON.h"
#include "wifi.h"
#include "mpu6050.h"
#include "kalmanfilter.h"
#include "mcpwm_motor.h"

/***
 @ MPU6050 I2c config
 */

static gpio_num_t i2c_gpio_sda = (gpio_num_t)1;
static gpio_num_t i2c_gpio_scl = (gpio_num_t)2;
#define I2C_NUM I2C_NUM_0

/***
 @ Using mcpwm with L298n driver
 */

#define L298n 1 // test code with l298n driver
#if L298n
#define GPIO_PWM0A_OUT 13
#define GPIO_PWM0B_OUT 25
#define dirMotorA1 12
#define dirMotorA2 14
#define dirMotorB1 27
#define dirMotorB2 26
#endif

cJSON *str_json, *str_TB1, *str_TB2, *str_C1, *str_C2, *usr;

int nhietdo = 0;
int doam = 0;
int TB1 = 0;
int TB2 = 0;
int C1 = 100;
int C2 = 200;
char JSON[100];
char Str_ND[100];
char Str_DA[100];
char Str_TB1[100];
char Str_TB2[100];
char Str_C1[100];
char Str_C2[100];
char Length[100];

// define function
void DataJson(unsigned int ND, unsigned int DA, unsigned int TB1, unsigned int TB2, unsigned int C1, unsigned int C2, unsigned int H1, unsigned int H2);
void delay(uint32_t time);

/*
@ Programing for Marvelmind
@
*/

UART::Uart Serial{UART_NUM_2, GPIO_NUM_17, GPIO_NUM_16};

// Main class used for testing only
class Main final
{
public:
    void run(void);
    void setup(void);

    WIFI::Wifi::state_e wifiState{WIFI::Wifi::state_e::NOT_INITIALIZED};
    WIFI::Wifi Wifi;

    esp_event_loop_handle_t gpio_loop_handle{};
    // LED pin on my NodeMCU
    CPPGPIO::GpioOutput cppLed{GPIO_NUM_27};
    // Repurpose the BOOT button to work as an input
    CPPGPIO::GpioInput cppButton{GPIO_NUM_0};
    // A second input to demonstrate Event_ID different event handlers
    CPPGPIO::GpioInput cppButton2{GPIO_NUM_12};

    // Event Handler for cppButton
    static void button_event_handler(void *handler_args, esp_event_base_t base, int32_t id, void *event_data);
    // Event Handler for cppButton2
    static void button2_event_handler(void *handler_args, esp_event_base_t base, int32_t id, void *event_data);
    // Event Handler for custom loop
    static void task_custom_event(void *handler_args, esp_event_base_t base, int32_t id, void *event_data);
    // Handle for the queue
    static xQueueHandle button_evt_queue;
    // Prototype for the task
    static void gpio_task_example(void *arg);

    void DataJson(unsigned int ND, unsigned int DA, unsigned int TB1, unsigned int TB2, unsigned int C1, unsigned int C2, unsigned int H1, unsigned int H2);

}; // Main Class

void delay(uint32_t time)
{
    vTaskDelay(time / portTICK_PERIOD_MS);
}

// Convert data to JSON
void Main::DataJson(unsigned int ND, unsigned int DA, unsigned int TB1, unsigned int TB2, unsigned int C1, unsigned int C2, unsigned int H1, unsigned int H2)
{

    char *dataJ;
    usr = cJSON_CreateObject();
    cJSON_AddItemToObject(usr, "ND", cJSON_CreateNumber(ND));
    cJSON_AddItemToObject(usr, "DA", cJSON_CreateNumber(DA));
    cJSON_AddItemToObject(usr, "TB1", cJSON_CreateNumber(TB1));
    cJSON_AddItemToObject(usr, "TB2", cJSON_CreateNumber(TB2));
    cJSON_AddItemToObject(usr, "C1", cJSON_CreateNumber(C1));
    cJSON_AddItemToObject(usr, "C2", cJSON_CreateNumber(C2));
    cJSON_AddItemToObject(usr, "H1", cJSON_CreateNumber(H1));
    cJSON_AddItemToObject(usr, "H2", cJSON_CreateNumber(H2));
    dataJ = cJSON_Print(usr);
    // printf("DataJ: %s\n", dataJ);

    static const char *TX_TASK_TAG = "TX_TASK";
    esp_log_level_set(TX_TASK_TAG, ESP_LOG_INFO);
    Serial.sendData(TX_TASK_TAG, dataJ);

    cJSON_Delete(usr);
    free(dataJ);

    // cJSON_AddItemToObject(usr, "C", cJSON_CreateString("Handsome"));

    // https://www.programmersought.com/article/98966009006/
}
