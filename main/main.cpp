
#include <iostream>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "main.h"

using namespace std;
xQueueHandle Main::button_evt_queue{nullptr};
Main App;

#define LOG_TAG "Main"
#define UART_TAG "UART_TAG"
static const int RX_BUF_SIZE = 1024;

/*
@ Programing for marvelmind_gps
*/

long hedgehog_x, hedgehog_y;  // coordinates of hedgehog (X,Y), mm
long hedgehog_z;              // height of hedgehog, mm
int hedgehog_pos_updated = 0; // flag of new data from hedgehog received

bool high_resolution_mode;
#define HEDGEHOG_BUF_SIZE 40
#define HEDGEHOG_CM_DATA_SIZE 0x10
#define HEDGEHOG_MM_DATA_SIZE 0x16

uint8_t hedgehog_serial_buf_ofs = 0;

#define POSITION_DATAGRAM_ID 0x0001         // cm resolution
#define POSITION_DATAGRAM_HIGHRES_ID 0x0011 // mm resolution
unsigned int hedgehog_data_id;

uint8_t hedgehog_address;
unsigned int paired_heading;

typedef union
{
    uint8_t b[2];
    unsigned int w;
    int wi;
} uni_8x2_16;
typedef union
{
    uint8_t b[4];
    float f;
    unsigned long v32;
    long vi32;
} uni_8x4_32;

uint16_t CalcCrcModbus_(uint8_t *buf, int len)
{
    uint16_t crc = 0xFFFF;
    int pos;
    for (pos = 0; pos < len; pos++)
    {
        crc ^= (uint16_t)buf[pos]; // XOR byte into least sig. byte of crc
        int i;
        for (i = 8; i != 0; i--) // Loop over each bit
        {
            if ((crc & 0x0001) != 0) // If the LSB is set
            {
                crc >>= 1; // Shift right and XOR 0xA001
                crc ^= 0xA001;
            }
            else           // Else LSB is not set
                crc >>= 1; // Just shift right
        }
    }
    return crc;
}

void chuongtrinhcambien(void)
{
    // DHT11 DHT2  , pt100 pt1000
    nhietdo++;
    doam = doam + 2;
}

void functionTransmitterUart(void *arg)
{

    static const char *TX_TASK_TAG = "TX_TASK";
    esp_log_level_set(TX_TASK_TAG, ESP_LOG_INFO);
    while (1)
    {
        // chuongtrinhcambien();
        // App.DataJson(nhietdo, doam, TB1, TB2, C1, C2, 10, 10);

        Serial.sendData(TX_TASK_TAG, "page0.t0.txt=\"hello world\"\xFF\xFF\xFF");

        delay(1000);
    }
    vTaskDelete(NULL);
}

void functionReceiveUart(void *arg)
{
    static const char *RX_TASK_TAG = "RX_TASK";
    esp_log_level_set(RX_TASK_TAG, ESP_LOG_INFO);

    uint8_t *hedgehog_serial_buf = (uint8_t *)malloc(30);

    /*
    @ Value to receive
    */
    int total_received_in_loop;
    int packet_received;
    uint8_t packet_size = 0;
    uni_8x2_16 un16;
    uni_8x4_32 un32;
    total_received_in_loop = 0;
    packet_received = 0;
    int rxBytes = 0;
    while (1)
    {

        const int rxBytes = Serial.readData(hedgehog_serial_buf);
        if (rxBytes > 0)
        {
            hedgehog_serial_buf[rxBytes] = 0;

            cout << " hedgehog position quality%: " << (int)hedgehog_serial_buf[6] << endl;
            cout << " hedgehog_address: " << (int)hedgehog_address << endl;
            packet_received = 0;
        }

        delay(50);
    }
    free(hedgehog_serial_buf);
    hedgehog_serial_buf = NULL;
    vTaskDelete(NULL);
}

/***
@ MPU get data
**/

static void mpu6050_task(void *pvParameters)
{
    MPU6050 mpu(i2c_gpio_scl, i2c_gpio_sda, I2C_NUM);

    if (!mpu.init())
    {
        ESP_LOGE("mpu6050", "init failed!");
        vTaskDelete(0);
    }
    ESP_LOGI("mpu6050", "init success!");

    float ax, ay, az, gx, gy, gz;
    float pitch, roll;
    float fpitch, froll;

    KALMAN pfilter(0.005);
    KALMAN rfilter(0.005);

    uint32_t lasttime = 0;
    int count = 0;

    while (1)
    {
        ax = -mpu.getAccX();
        ay = -mpu.getAccY();
        az = -mpu.getAccZ();
        gx = mpu.getGyroX();
        gy = mpu.getGyroY();
        gz = mpu.getGyroZ();
        pitch = atan(ax / az) * 57.2958;
        roll = atan(ay / az) * 57.2958;
        fpitch = pfilter.filter(pitch, gy);
        froll = rfilter.filter(roll, -gx);
        count++;
        if (esp_log_timestamp() / 1000 != lasttime)
        {
            lasttime = esp_log_timestamp() / 1000;
            printf("Samples:%d ", count);
            count = 0;
            printf(" Acc:(%4.2f,%4.2f,%4.2f)", ax, ay, az);
            printf("Gyro:(%6.3f,%6.3f,%6.3f)", gx, gy, gz);
            printf(" Pitch:%6.3f ", pitch);
            printf(" Roll:%6.3f ", roll);
            printf(" FPitch:%6.3f ", fpitch);
            printf(" FRoll:%6.3f \n", froll);
        }
    }
}

void Main::run(void)
{
    // wifiState = Wifi.GetState();
    // switch (wifiState)
    // {
    // case WIFI::Wifi::state_e::READY_TO_CONNECT:
    //     std::cout << "Wifi Status: READY_TO_CONNECT\n";
    //     Wifi.Begin();
    //     break;
    // case WIFI::Wifi::state_e::DISCONNECTED:
    //     std::cout << "Wifi Status: DISCONNECTED\n";
    //     Wifi.Begin();
    //     break;
    // case WIFI::Wifi::state_e::CONNECTING:
    //     std::cout << "Wifi Status: CONNECTING\n";
    //     break;
    // case WIFI::Wifi::state_e::WAITING_FOR_IP:
    //     std::cout << "Wifi Status: WAITING_FOR_IP\n";
    //     break;
    // case WIFI::Wifi::state_e::ERROR:
    //     std::cout << "Wifi Status: ERROR\n";
    //     break;
    // case WIFI::Wifi::state_e::CONNECTED:
    //     std::cout << "Wifi Status: CONNECTED\n";
    //     break;
    // case WIFI::Wifi::state_e::NOT_INITIALIZED:
    //     std::cout << "Wifi Status: NOT_INITIALIZED\n";
    //     break;
    // case WIFI::Wifi::state_e::INITIALIZED:
    //     std::cout << "Wifi Status: INITIALIZED\n";
    //     break;
    // }
    cppLed.setLevel(cppButton.read());
    delay(200);
}
void Main::setup(void)
{
    ESP_LOGI(LOG_TAG, "Setup!");

    // Wifi
    esp_event_loop_create_default(); // Create System Event Loop

    nvs_flash_init();

    // Wifi.SetCredentials("anhquan123", "quan1234");
    Wifi.Init();
    Wifi.Begin();

    // Uart
    Serial.initUart();

    // Serial.sendData("ngoinhaiot.com");

    // xTaskCreate(functionTransmitterUart, "uart_tx", 2048 * 2, NULL, 3, NULL);

    xTaskCreate(functionReceiveUart, "uart_rx", 2048 * 2, NULL, 3, NULL);

    // GPIO
    cppLed.on();
    button_evt_queue = xQueueCreate(10, sizeof(uint32_t));
    xTaskCreate(gpio_task_example, "gpio_task_example", 2048, NULL, 10, NULL);

    esp_event_loop_args_t gpio_loop_args;
    gpio_loop_args.queue_size = 5;
    gpio_loop_args.task_name = "loop_task"; // Task will be created
    gpio_loop_args.task_priority = uxTaskPriorityGet(NULL);
    gpio_loop_args.task_stack_size = 2048;
    gpio_loop_args.task_core_id = tskNO_AFFINITY;

    esp_event_loop_create(&gpio_loop_args, &gpio_loop_handle); // Create Custom Event Loop

    cppButton.enableInterrupt(GPIO_INTR_NEGEDGE);
    cppButton.setEventHandler(&button_event_handler);

    cppButton2.enablePullup();
    cppButton2.enableInterrupt(GPIO_INTR_NEGEDGE);
    cppButton2.setEventHandler(&button2_event_handler);
}

extern "C" void app_main(void)
{

    App.setup();
    delay(1000);
    while (true)
    {
        App.run();
    }
}

void Main::button_event_handler(void *handler_args, esp_event_base_t base, int32_t id, void *event_data)
{
    std::cout << "Button triggered interrupt with ID: " << id << '\n';
}

void Main::button2_event_handler(void *handler_args, esp_event_base_t base, int32_t id, void *event_data)
{
    std::cout << "Button triggered interrupt with ID: " << id << '\n';
}

void Main::task_custom_event(void *handler_args, esp_event_base_t base, int32_t id, void *event_data)
{
    std::cout << "Button triggered interrupt with ID: " << id << " With Custom Loop\n";
}

void Main::gpio_task_example(void *arg)
{
    uint32_t io_num;
    for (;;)
    {
        if (xQueueReceive(Main::button_evt_queue, &io_num, portMAX_DELAY))
        {
            std::cout << "Interrupt triggered from pin " << (int)io_num << " and send to queue\n";
        }
    }
}
