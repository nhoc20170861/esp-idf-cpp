#pragma once

// include library
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "esp_log.h"

#define UART_RTS (UART_PIN_NO_CHANGE)
#define UART_CTS (UART_PIN_NO_CHANGE)

namespace UART
{
    class Uart
    {
    protected:
        uart_port_t _UART_PORT = UART_NUM_0;
        int _pin_UART_TXD;
        int _pin_UART_RXD;
        static const int BUF_SIZE = 1024;
        const uart_config_t uart_config = {
            .baud_rate = 115200,
            .data_bits = UART_DATA_8_BITS,
            .parity = UART_PARITY_DISABLE,
            .stop_bits = UART_STOP_BITS_1,
            .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
            .rx_flow_ctrl_thresh = 122,
            .source_clk = UART_SCLK_APB,
        };
        esp_err_t _initUart();

    public:
        Uart(void);
        Uart(uart_port_t UART_PORT, const gpio_num_t pin_UART_TXD, const gpio_num_t pin_UART_RXD);
        esp_err_t initUart();
        void sendData(const char* logName, const char *data);
        int readData(uint8_t *data);
    }; // Uart Class
}
