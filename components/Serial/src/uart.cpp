#include "Uart.h"

static const char *UART_TAG = "UART TEST";

// void initUart(void)
// {

//     const uart_config_t uart_config = {
//         .baud_rate = 115200,
//         .data_bits = UART_DATA_8_BITS,
//         .parity = UART_PARITY_DISABLE,
//         .stop_bits = UART_STOP_BITS_1,
//         .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
//         .rx_flow_ctrl_thresh = 122,
//         .source_clk = UART_SCLK_APB,
//     };

//     // Configure UART parameters
//     ESP_ERROR_CHECK(uart_param_config(UART_PORT, &uart_config));

//     // Uart driver install
//     ESP_ERROR_CHECK(uart_driver_install(UART_PORT, BUF_SIZE * 2, BUF_SIZE * 2, 0, NULL, 0));

//     // Set UART log level
//     esp_log_level_set(UART_TAG, ESP_LOG_INFO);
//     ESP_LOGI(UART_TAG, "UART set pins, mode and install driver.");

//     // Uart set_pin
//     uart_set_pin(UART_PORT, UART_TXD2, UART_RXD2, UART_RTS, UART_CTS);

//     //uart_enable_pattern_det_baud_intr(UART_PORT, '`', 1, USHRT_MAX, 0, 0);
// }

// void sendData(const char *data)
// {
//     const int len = strlen(data);
//     uart_write_bytes(UART_PORT, data, len);
//     ESP_LOGI(UART_TAG, "Data Send: %s\n", data);
// }
namespace UART
{
    Uart::Uart(void)
    {
    }
    Uart::Uart(uart_port_t UART_PORT, const gpio_num_t pin_UART_TXD, const gpio_num_t pin_UART_RXD)
    {
        _UART_PORT = UART_PORT;
        _pin_UART_TXD = pin_UART_TXD;
        _pin_UART_RXD = pin_UART_RXD;
    }

    esp_err_t Uart::initUart()
    {
        return _initUart();
    }
    esp_err_t Uart::_initUart()
    {
        esp_err_t status{ESP_OK};
        esp_log_level_set(UART_TAG, ESP_LOG_INFO);
        ESP_LOGI(UART_TAG, "UART set pins, mode and install driver.");

        // Uart driver install
        status |= uart_driver_install(_UART_PORT, BUF_SIZE * 2, 0, 0, NULL, 0);

        // Configure UART parameters
        status |= uart_param_config(_UART_PORT, &uart_config);

        if (_UART_PORT != UART_NUM_0)
        {

            uart_set_pin(_UART_PORT, _pin_UART_TXD, _pin_UART_RXD, UART_RTS, UART_CTS);
        }

        return status;
    }
    void Uart::sendData(const char *logName, const char *data)
    {
        const int len = strlen(data);
        uart_write_bytes(_UART_PORT, data, len);
        // ESP_LOGI(logName, "Data Send: %s\n", data);
    }

    int Uart::readData(uint8_t *data)
    {
        int rxBytes = 0;
        ESP_ERROR_CHECK(uart_get_buffered_data_len(_UART_PORT, (size_t *)&rxBytes));
        rxBytes = uart_read_bytes(_UART_PORT, data, rxBytes, 20 / portTICK_RATE_MS);
        return rxBytes;
    }

}
