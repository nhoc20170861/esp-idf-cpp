#pragma once

#include <cstring>

#include <mutex>
#include "esp_log.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_smartconfig.h"
#include "smartconfig_ack.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
namespace WIFI
{
    class Wifi
    {
        constexpr static const char *_log_tag{"WiFi"};
        static const int CONNECTED_BIT = BIT0;
        static const int ESPTOUCH_DONE_BIT = BIT1;

    public:
        // Wifi status
        enum class state_e
        {
            NOT_INITIALIZED,
            INITIALIZED,
            READY_TO_CONNECT,
            CONNECTING,
            WAITING_FOR_IP,
            CONNECTED,
            DISCONNECTED,
            ERROR
        };

        // For smartconfig Wifi
        enum class smartconfig_state_e
        {
            NOT_STARTED,
            STARTED
        };

    private:
        static smartconfig_state_e smartconfig_state;
        static smartconfig_start_config_t smartconfig_config;
        static bool empty_credentials(void)
        {
            if ('\0' == wifi_config.sta.ssid[0] ||
                '\0' == wifi_config.sta.password[0])
                return true;
            return false;
        }

        static esp_err_t _init(void);
        static wifi_init_config_t _wifi_init_cfg; // This variable to hold the wifi initialization configuration data
        static wifi_config_t wifi_config;         // This variable will hold the Wifi configuration data

        // An event handler that will trigger whenever the state of the wifi changes.
        static void wifi_event_handler(void *arg, esp_event_base_t event_base,
                                       int32_t event_id, void *event_data);

        // An event handler that will use smartconfig
        static void ip_event_handler(void *arg, esp_event_base_t event_base,
                                     int32_t event_id, void *event_data);

        // An event handler that will use smartconfig
        static void sc_event_handler(void *arg, esp_event_base_t event_base,
                                     int32_t event_id, void *event_data);

        static state_e _state;
        static esp_err_t _get_mac(void);
        static char _mac_addr_cstr[13];
        static std::mutex _mutx;

    public:
        Wifi(void);

        void SetCredentials(const char *ssid, const char *password);

        esp_err_t Init();
        esp_err_t Begin(void);

        constexpr static const state_e &GetState(void) { return _state; }
        constexpr static const char *GetMac(void) { return _mac_addr_cstr; }

        static void smartconfig_example_task(void *parm);

    }; // Wifi class

} // namaspace WIFI