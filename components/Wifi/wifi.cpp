#include "Wifi.h"

namespace WIFI
{
    // Statics
    char Wifi::_mac_addr_cstr[]{};
    std::mutex Wifi::_mutx{};
    Wifi::state_e Wifi::_state{state_e::NOT_INITIALIZED};
    wifi_init_config_t Wifi::_wifi_init_cfg = WIFI_INIT_CONFIG_DEFAULT();
    wifi_config_t Wifi::wifi_config{};
    Wifi::smartconfig_state_e Wifi::smartconfig_state{smartconfig_state_e::NOT_STARTED};
    // smartconfig_start_config_t Wifi::smartconfig_config = SMARTCONFIG_START_CONFIG_DEFAULT();
    smartconfig_start_config_t Wifi::smartconfig_config{true, false, nullptr};
    // Wifi Contrustor
    Wifi::Wifi(void)
    {
        if (!_mac_addr_cstr[0])
        {
            if (ESP_OK != _get_mac())
            {
                esp_restart();
            }
        }
    }

    void Wifi::wifi_event_handler(void *arg, esp_event_base_t event_base,
                                  int32_t event_id, void *event_data)
    {
        if (WIFI_EVENT == event_base)
        {
            const wifi_event_t event_type{static_cast<wifi_event_t>(event_id)};

            ESP_LOGI(_log_tag, "%s:%d Event ID %d", __func__, __LINE__, event_id);
            switch (event_type)
            {
            case WIFI_EVENT_STA_START:
            {
                ESP_LOGI(_log_tag, "%s:%d STA_START, waiting for state_mutx", __func__, __LINE__);
                std::lock_guard<std::mutex> state_guard(_mutx);
                _state = state_e::READY_TO_CONNECT;
                ESP_LOGI(_log_tag, "%s:%d READY_TO_CONNECT", __func__, __LINE__);
                break;
            }

            case WIFI_EVENT_STA_CONNECTED:
            {
                ESP_LOGI(_log_tag, "%s:%d STA_CONNECTED, waiting for state_mutx", __func__, __LINE__);
                std::lock_guard<std::mutex> state_guard(_mutx);
                _state = state_e::WAITING_FOR_IP;
                ESP_LOGI(_log_tag, "%s:%d WAITING_FOR_IP", __func__, __LINE__);
                break;
            }

            case WIFI_EVENT_STA_DISCONNECTED:
            {
                std::lock_guard<std::mutex> state_guard(_mutx);
                _state = state_e::DISCONNECTED;
                ESP_LOGW(_log_tag, "%s:%d STA_DISCONNECTED (%d)", __func__, __LINE__, event_id);
                break;
            }

            default:
                break;
            }
        }
    }

    void Wifi::ip_event_handler(void *arg, esp_event_base_t event_base,
                                int32_t event_id, void *event_data)
    {
        if (IP_EVENT == event_base)
        {
            const ip_event_t event_type{static_cast<ip_event_t>(event_id)};

            switch (event_type)
            {
            case IP_EVENT_STA_GOT_IP:
            {
                std::lock_guard<std::mutex> state_guard(_mutx);
                _state = state_e::CONNECTED;
                break;
            }

            case IP_EVENT_STA_LOST_IP:
            {
                std::lock_guard<std::mutex> state_guard(_mutx);
                if (state_e::DISCONNECTED != _state)
                {
                    _state = state_e::WAITING_FOR_IP;
                }
                break;
            }

            default:
                break;
            }
        }
    }

    void Wifi::sc_event_handler(void *arg, esp_event_base_t event_base,
                                int32_t event_id, void *event_data)
    {

        esp_err_t status{ESP_OK};

        if (SC_EVENT == event_base)
        {
            const smartconfig_event_t event_type{static_cast<smartconfig_event_t>(event_id)};
            ESP_LOGI(_log_tag, "%s:%d Event ID %d", __func__, __LINE__, event_id);
            switch (event_type)
            {

            case SC_EVENT_SCAN_DONE:
            {
                ESP_LOGI(_log_tag, "Scan done");
                break;
            }
            case SC_EVENT_FOUND_CHANNEL:
            {
                ESP_LOGI(_log_tag, "Found channel");
                break;
            }
            case SC_EVENT_GOT_SSID_PSWD:
            {

                ESP_LOGI(_log_tag, "Got SSID and password");
                const smartconfig_event_got_ssid_pswd_t *const evt = static_cast<smartconfig_event_got_ssid_pswd_t *>(event_data);
                wifi_config_t wifi_config;
                uint8_t ssid[33] = {0};
                uint8_t password[65] = {0};
                uint8_t rvd_data[33] = {0};

                // Get info ssid/password to connect wifi
                if (_state == state_e::READY_TO_CONNECT)
                {

                    bzero(&wifi_config, sizeof(wifi_config_t));

                    memcpy(wifi_config.sta.ssid, evt->ssid, std::min(sizeof(evt->ssid), sizeof(wifi_config.sta.ssid)));
                    memcpy(wifi_config.sta.password, evt->password, std::min(sizeof(evt->password), sizeof(wifi_config.sta.password)));

                    wifi_config.sta.bssid_set = evt->bssid_set;
                    if (wifi_config.sta.bssid_set == true)
                    {
                        memcpy(wifi_config.sta.bssid, evt->bssid, sizeof(wifi_config.sta.bssid));
                    }

                    memcpy(ssid, evt->ssid, sizeof(evt->ssid));
                    memcpy(password, evt->password, sizeof(evt->password));
                    ESP_LOGI(_log_tag, "SSID: %s", wifi_config.sta.ssid);
                    ESP_LOGI(_log_tag, "PASSWORD: %s", wifi_config.sta.password);
                    if (evt->type == SC_TYPE_ESPTOUCH_V2)
                    {
                        ESP_ERROR_CHECK(esp_smartconfig_get_rvd_data(rvd_data, sizeof(rvd_data)));
                        ESP_LOGI(_log_tag, "RVD_DATA:");
                        for (int i = 0; i < 33; i++)
                        {
                            printf("%02x ", rvd_data[i]);
                        }
                        printf("\n");
                    }

                    ESP_LOGI(_log_tag, "%s Calling esp_wifi_connect", __func__);
                    status = esp_wifi_set_config(WIFI_IF_STA, &wifi_config);
                    status = esp_wifi_connect();
                    if (ESP_OK == status)
                    {
                        ESP_LOGI(_log_tag, "%s: %d esp connecting wifi", __func__, __LINE__);
                        _state = state_e::CONNECTING;
                    }
                }

                break;
            }
            case SC_EVENT_SEND_ACK_DONE:
            {
                ESP_LOGI(_log_tag, "%s: %d Esp_smartconfig done!!!", __func__, __LINE__);
            }

            default:
                break;
            }
        }
    }

    esp_err_t Wifi::Begin(void)
    {
        std::lock_guard<std::mutex> connect_guard(_mutx);

        esp_err_t status{ESP_OK};

        switch (_state)
        {
        case state_e::READY_TO_CONNECT:
            if (!empty_credentials())
            {
                ESP_LOGI(_log_tag, "%s:%d Calling esp_wifi_connect ", __func__, __LINE__);
                status = esp_wifi_connect();
                ESP_LOGI(_log_tag, "%s:%d Esp_wifi_connect: %s", __func__, __LINE__, esp_err_to_name(status));
                if (ESP_OK == status)
                    _state = state_e::CONNECTING;
            }
            else if (smartconfig_state_e::NOT_STARTED == smartconfig_state)
            {

                ESP_ERROR_CHECK(esp_smartconfig_set_type(SC_TYPE_ESPTOUCH));
                status = esp_smartconfig_start(&smartconfig_config);
                ESP_LOGI(_log_tag, "%s:%d esp_smartconfig: %s", __func__, __LINE__, esp_err_to_name(status));
                if (ESP_OK == status)
                    smartconfig_state = smartconfig_state_e::STARTED;
            }
            break;

        case state_e::DISCONNECTED:
        {
            if (!empty_credentials())
            {
                if (smartconfig_state_e::STARTED == smartconfig_state)
                {
                    ESP_LOGI(_log_tag, "%s:%d esp32 reconnecting ", __func__, __LINE__);
                    status = esp_wifi_connect();
                    ESP_LOGI(_log_tag, "%s:%d esp_wifi_connect: %s", __func__, __LINE__, esp_err_to_name(status));
                    if (ESP_OK == status)
                    {
                        _state = state_e::CONNECTING;
                        smartconfig_state = smartconfig_state_e::NOT_STARTED;
                    }
                }
                else if (smartconfig_state_e::NOT_STARTED == smartconfig_state)
                {
                    ESP_ERROR_CHECK(esp_smartconfig_set_type(SC_TYPE_ESPTOUCH));
                    status = esp_smartconfig_start(&smartconfig_config);
                    ESP_LOGI(_log_tag, "%s:%d esp_smartconfig: %s", __func__, __LINE__, esp_err_to_name(status));
                    if (ESP_OK == status)
                        smartconfig_state = smartconfig_state_e::STARTED;
                }
            }
            break;
        }
        case state_e::CONNECTING:
        case state_e::WAITING_FOR_IP:
        case state_e::CONNECTED:
            break;
        case state_e::NOT_INITIALIZED:
        case state_e::INITIALIZED:
        case state_e::ERROR:
            status = ESP_FAIL;
            break;
        }
        return status;
    }

    esp_err_t Wifi::_init()
    {
        std::lock_guard<std::mutex> mutx_guard(_mutx);

        esp_err_t status{ESP_OK};

        if (state_e::NOT_INITIALIZED == _state)
        {
            status |= esp_netif_init();
            if (ESP_OK == status)
            {
                const esp_netif_t *const p_netif = esp_netif_create_default_wifi_sta();
                assert(p_netif);

                if (!p_netif)
                {
                    status = ESP_FAIL;
                }
            }

            if (ESP_OK == status)
            {
                status = esp_wifi_init(&_wifi_init_cfg);
            }

            if (ESP_OK == status)
            {
                status = esp_event_handler_instance_register(WIFI_EVENT,
                                                             ESP_EVENT_ANY_ID,
                                                             &wifi_event_handler,
                                                             nullptr,
                                                             nullptr);
            }

            if (ESP_OK == status)
            {
                status = esp_event_handler_instance_register(IP_EVENT,
                                                             ESP_EVENT_ANY_ID,
                                                             &ip_event_handler,
                                                             nullptr,
                                                             nullptr);
            }
            if (ESP_OK == status)
            {
                status = esp_event_handler_instance_register(SC_EVENT,
                                                             ESP_EVENT_ANY_ID,
                                                             &sc_event_handler,
                                                             nullptr,
                                                             nullptr);
            }

            if (ESP_OK == status)
            {
                status = esp_wifi_set_mode(WIFI_MODE_STA);
            }

            if (ESP_OK == status)
            {
                wifi_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
                wifi_config.sta.pmf_cfg.capable = true;
                wifi_config.sta.pmf_cfg.required = false;

                status = esp_wifi_set_config(WIFI_IF_STA, &wifi_config);
            }

            if (ESP_OK == status)
            {
                status = esp_wifi_start(); // start Wifi
            }

            if (ESP_OK == status)
            {
                _state = state_e::INITIALIZED;
            }

            ESP_LOGI(_log_tag, "%s:%d esp_init: %s", __func__, __LINE__, esp_err_to_name(status));
        }

        else if (state_e::ERROR == _state)
        {
            _state = state_e::NOT_INITIALIZED;
        }

        return status;
    }

    void Wifi::SetCredentials(const char *ssid, const char *password)
    {
        memcpy(wifi_config.sta.ssid, ssid, std::min(strlen(ssid), sizeof(wifi_config.sta.ssid)));

        memcpy(wifi_config.sta.password, password, std::min(strlen(password), sizeof(wifi_config.sta.password)));
    }

    esp_err_t Wifi::Init()
    {
        return _init();
    }

    // Get default MAC from API and convert to ASCII HEX
    esp_err_t Wifi::_get_mac(void)
    {
        uint8_t mac_byte_buffer[6]{};

        const esp_err_t status{esp_efuse_mac_get_default(mac_byte_buffer)};

        if (ESP_OK == status)
        {
            snprintf(_mac_addr_cstr, sizeof(_mac_addr_cstr), "%02X%02X%02X%02X%02X%02X",
                     mac_byte_buffer[0],
                     mac_byte_buffer[1],
                     mac_byte_buffer[2],
                     mac_byte_buffer[3],
                     mac_byte_buffer[4],
                     mac_byte_buffer[5]);
        }

        return status;
    }

} // namespace WIFI