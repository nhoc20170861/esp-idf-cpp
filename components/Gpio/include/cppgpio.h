#pragma once

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_event.h"
#include "driver/gpio.h"

namespace CPPGPIO
{
    ESP_EVENT_DECLARE_BASE(INPUT_EVENTS);

    class GpioBase
    {
    protected:
        bool _active_low = false;
    }; // GpioBase Class

    class GpioInput : public GpioBase
    {
    private:
        esp_err_t _init(const gpio_num_t pin, const bool activeLow);
        static bool _interrupt_service_installed;

        esp_event_handler_t _event_handle = nullptr;
        static portMUX_TYPE _eventChangeMutex;

        // create a struct for the ISR to have a access to the data
        struct interrupt_args
        {
            bool _event_handler_set = false;        // A boolean flag the tracks whether or not a default event handler has been set.
            bool _custom_event_handler_set = false; // A boolean flag to indicate whether or not a user-created event loop is being used
            bool _queue_enabled = false;            // A boolean flag to indicate if a queue will be used
            gpio_num_t _pin;                        // The variable indicating the input pin on this instance
            esp_event_loop_handle_t _custom_event_loop_handle {nullptr}; // The handle for the custom event loop if used
            xQueueHandle _queue_handle {nullptr};   // The handle to the queue if a queue is used
        } _interrupt_args;

    public:
        GpioInput(const gpio_num_t pin, const bool activeLow);
        GpioInput(const gpio_num_t pin);
        GpioInput(void);
        esp_err_t init(const gpio_num_t pin, const bool activeLow);
        esp_err_t init(const gpio_num_t pin);
        int read(void);

        esp_err_t _clearEventHandlers(); // This method will unregister any events registered for the specific input 

        esp_err_t enablePullup(void);
        esp_err_t disablePullup(void);
        esp_err_t enablePulldown(void);
        esp_err_t disablePulldown(void);
        esp_err_t enablePullupPulldown(void);
        esp_err_t disablePullupPulldown(void);
        
        esp_err_t enableInterrupt(gpio_int_type_t int_type);
        esp_err_t setEventHandler(esp_event_handler_t Gpio_e_h);
        esp_err_t setEventHandler(esp_event_loop_handle_t Gpio_e_l, esp_event_handler_t Gpio_e_h); // for user custom
        void setQueueHandle(xQueueHandle Gpio_e_q);                                                // This function accpets a handle to a queue
        static void IRAM_ATTR gpio_isr_callback(void* arg);
    }; // GpioInput Class

    class GpioOutput : public GpioBase
    {
    private:
        gpio_num_t _pin;
        int _level = 0;
        esp_err_t _init(const gpio_num_t pin, const bool activeLow);

    public:
        GpioOutput(const gpio_num_t pin, const bool activeLow);
        GpioOutput(const gpio_num_t pin);
        GpioOutput(void);
        esp_err_t init(const gpio_num_t pin, const bool activeLow);
        esp_err_t init(const gpio_num_t pin);
        esp_err_t on(void);
        esp_err_t off(void);
        esp_err_t toggle(void);
        esp_err_t setLevel(int level);
    }; // GpioOutput Class
} // Gpio Namespace